// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterStatComponent.h"
#include "FirstGameInstance.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UCharacterStatComponent::UCharacterStatComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;

	Level = 1;
}


// Called when the game starts
void UCharacterStatComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UCharacterStatComponent::InitializeComponent()
{
	Super::InitializeComponent();
	SetLevel(Level);
}

void UCharacterStatComponent::SetLevel(int32 NewLevel)
{
	auto GameInstance = Cast<UFirstGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	if (GameInstance)
	{
		auto StatData = GameInstance->GetStatData(NewLevel);

		if (StatData)
		{
			Level = StatData->Level;
			HP = StatData->MaxHP;
			AttackDamage = StatData->AttackDamage;
		}
	}
	
}

void UCharacterStatComponent::OnAttacked(int32 DamageAmount)
{
	HP -= DamageAmount;
	if (HP < 0) {HP = 0;}

	UE_LOG(LogTemp, Warning, TEXT("Remaining Damage: %d"), HP);
}

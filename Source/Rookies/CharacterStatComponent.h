// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterStatComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ROOKIES_API UCharacterStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterStatComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;

	
public:
	void SetLevel(int32 Level);
	void OnAttacked(int32 DamageAmount);

	int32 GetLevel() {return Level;}
	int32 GetHP() {return HP;}
	int32 GetAttackDamage() {return AttackDamage;}

private:
	UPROPERTY(EditAnywhere, Category = Stat, meta=(AllowPrivateAccess))
	int32 Level;

	UPROPERTY(EditAnywhere, Category = Stat, meta=(AllowPrivateAccess))
	int32 AttackDamage;

	UPROPERTY(EditAnywhere, Category = Stat, meta=(AllowPrivateAccess))
	int32 HP;

};

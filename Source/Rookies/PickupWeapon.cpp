// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupWeapon.h"
#include "GreyStoneCharacter.h"

// Sets default values
APickupWeapon::APickupWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Weapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM(TEXT("StaticMesh'/Game/ParagonGreystone/FX/Meshes/Heroes/Greystone/SM_Greystone_Blade_01.SM_Greystone_Blade_01'"));
	if (SM.Succeeded())
	{
		Weapon->SetStaticMesh(SM.Object);
	}
	Weapon->SetupAttachment(RootComponent);
	Trigger->SetupAttachment(Weapon);
	
	Weapon->SetCollisionProfileName(TEXT("PickUp"));
	Trigger->SetCollisionProfileName(TEXT("PickUp"));
	Trigger->SetBoxExtent(FVector(30.f,30.f,30.f));
}

// Called when the game starts or when spawned
void APickupWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickupWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	Trigger->OnComponentBeginOverlap.AddDynamic(this, &APickupWeapon::OnCharacterOverlap);
}

void APickupWeapon::OnCharacterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AGreyStoneCharacter* HitCharacter = Cast<AGreyStoneCharacter>(OtherActor);
	if (HitCharacter)
	{
		FName WeaponSocket(TEXT("BackSlot"));
		if (HitCharacter)
		{
			AttachToComponent(HitCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponSocket);
		}
	}
}



// Fill out your copyright notice in the Description page of Project Settings.


#include "GreyStoneCharacter.h"

#include "CharacterAnimInstance.h"
#include "CharacterStatComponent.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"
#include "PickupWeapon.h"

// Sets default values
AGreyStoneCharacter::AGreyStoneCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	SpringArm->SetupAttachment(GetCapsuleComponent());
	Camera->SetupAttachment(SpringArm);

	SpringArm->TargetArmLength = 500.f;
	SpringArm->SetRelativeRotation(FRotator(-35.f, 0.f, 0.f));

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -88.f), FRotator(0.f,-90.f,0.f)); 
	
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SMesh(TEXT("SkeletalMesh'/Game/ParagonGreystone/Characters/Heroes/Greystone/Skins/Tough/Meshes/Greystone_Tough.Greystone_Tough'"));
	if(SMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SMesh.Object);
	}

	StatComponent = CreateDefaultSubobject<UCharacterStatComponent>(TEXT("Stats"));
}

// Called when the game starts or when spawned
void AGreyStoneCharacter::BeginPlay()
{
	Super::BeginPlay();

	
}

void AGreyStoneCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AnimInstance = Cast<UCharacterAnimInstance>(GetMesh()->GetAnimInstance());
	if (AnimInstance)
	{
		AnimInstance->OnMontageEnded.AddDynamic(this, &AGreyStoneCharacter::OnAttackMontage);
		AnimInstance->OnAttackHit.AddUObject(this, &AGreyStoneCharacter::AttackCheck);
	}
}

// Called every frame
void AGreyStoneCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGreyStoneCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("UpDown"), this, &AGreyStoneCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("LeftRight"), this, &AGreyStoneCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("Yaw"), this, &APawn::AddControllerYawInput);
	
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Attack"), IE_Pressed, this, &AGreyStoneCharacter::Attack);
	

}

float AGreyStoneCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	StatComponent->OnAttacked(DamageAmount);

	return DamageAmount;
}

void AGreyStoneCharacter::MoveForward(float InputValue)
{
	UpDownValue = InputValue;
	AddMovementInput(GetActorForwardVector(), InputValue);
	
}

void AGreyStoneCharacter::MoveRight(float InputValue)
{
	LeftRightValue = InputValue;
	AddMovementInput(GetActorRightVector(), InputValue);
}

void AGreyStoneCharacter::Attack()
{
	if(IsAttacking) return;
	
	AnimInstance = Cast<UCharacterAnimInstance>(GetMesh()->GetAnimInstance());
	if (AnimInstance)
	{
		AnimInstance->PlayAttackMontage();
		AnimInstance->JumpToSection(AttackIndex);
		
		AttackIndex = (AttackIndex+1) % 3;
		
	}
	IsAttacking = true;
	//UE_LOG(LogTemp, Warning, TEXT("%d"), AttackIndex);
}

void AGreyStoneCharacter::OnAttackMontage(UAnimMontage* Montage, bool bInterrupted)
{
	IsAttacking = false;
}

void AGreyStoneCharacter::AttackCheck()
{
	FHitResult HitResult;
	FCollisionQueryParams Params(NAME_None, false, this);
	float AttackRange = 100.f;
	float AttackRadius = 50.f;
	
	bool bResult = GetWorld()->SweepSingleByChannel(HitResult,
		GetActorLocation(),
		GetActorLocation() + (GetActorForwardVector()*AttackRange),
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeSphere(AttackRadius),
		Params
		);
	// 여기까지 TraceChannel + 앞으로 디버깅용 sphere

	FVector Vec = GetActorForwardVector() * AttackRange;
	FVector Center = GetActorLocation() + (Vec *0.5f);
	float HalfHeight = AttackRange * 0.5f + AttackRadius;
	FQuat Rotation = FRotationMatrix::MakeFromZ(Vec).ToQuat();
	FColor DrawColor;

	if(bResult && IsValid(HitResult.GetActor()))
	{
		UE_LOG(LogTemp, Log, TEXT("Hit Actor: %s"), *HitResult.GetActor()->GetName());
		DrawColor = FColor::Green;

		FDamageEvent DamageEvent;
		HitResult.GetActor()->TakeDamage(StatComponent->GetAttackDamage(), DamageEvent, GetController(), this);
		
	}
	else
	{
		DrawColor = FColor::Red;
	}
	DrawDebugCapsule(GetWorld(), Center, HalfHeight, AttackRadius, Rotation, DrawColor, false, 2.f);
}


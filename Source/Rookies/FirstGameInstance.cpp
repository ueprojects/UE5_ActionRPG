// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstGameInstance.h"

UFirstGameInstance::UFirstGameInstance()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> DATA(TEXT("DataTable'/Game/Data/CharacterStatTable.CharacterStatTable'"));
	if (DATA.Succeeded())
	{
		CharacterStats = DATA.Object;		
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No DataTable found"));
	}

}

void UFirstGameInstance::Init()
{
	Super::Init();

	UE_LOG(LogTemp, Log, TEXT("Attack Damage: %d"), GetStatData(1)->AttackDamage);
}

FCharacterData* UFirstGameInstance::GetStatData(int32 Level) const
{
	return CharacterStats->FindRow<FCharacterData>(*FString::FromInt(Level), TEXT(""));
}

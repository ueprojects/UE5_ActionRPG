// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Rookies : ModuleRules
{
	public Rookies(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}

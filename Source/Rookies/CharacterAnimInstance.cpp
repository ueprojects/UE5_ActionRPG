// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAnimInstance.h"

#include "GreyStoneCharacter.h"
#include "GameFramework/Character.h"
#include "GameFramework/PawnMovementComponent.h"

UCharacterAnimInstance::UCharacterAnimInstance()
{
	static ConstructorHelpers::FObjectFinder<UAnimMontage> AMontage(TEXT("AnimMontage'/Game/Animations/Greystone_Skeleton_Montage.Greystone_Skeleton_Montage'"));
	if (AMontage.Succeeded())
	{
		AttackMontage = AMontage.Object;
	}
}

void UCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto PawnOwner = TryGetPawnOwner();
	if (IsValid(PawnOwner))
	{
		Speed = PawnOwner->GetVelocity().Size();

		auto Character = Cast<AGreyStoneCharacter>(PawnOwner);
		if (IsValid(Character))
		{
			IsFalling = Character->GetMovementComponent()->IsFalling();

			Vertical = Character->UpDownValue;
			Horizontal = Character->LeftRightValue;
		}
	}
}

void UCharacterAnimInstance::PlayAttackMontage()
{
	Montage_Play(AttackMontage);
}

void UCharacterAnimInstance::JumpToSection(int32 SectionIndex)
{
	FName Name = GetAttackMontageName(SectionIndex);
	Montage_JumpToSection(Name, AttackMontage);
}

FName UCharacterAnimInstance::GetAttackMontageName(int32 SectionIndex)
{
	return FName(FString::Printf(TEXT("Attack%d"), SectionIndex));
} 

void UCharacterAnimInstance::AnimNotify_AttackHit()
{
	OnAttackHit.Broadcast();
}

// Copyright Epic Games, Inc. All Rights Reserved.

#include "RookiesGameMode.h"
#include "RookiesCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARookiesGameMode::ARookiesGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

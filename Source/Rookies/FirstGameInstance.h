// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "FirstGameInstance.generated.h"

/**
 * 
 */

USTRUCT()
struct FCharacterData : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AttackDamage;
	
};

UCLASS()
class ROOKIES_API UFirstGameInstance : public UGameInstance
{
	GENERATED_BODY()

	UFirstGameInstance();

	virtual void Init() override;

public:

	FCharacterData* GetStatData(int32 Level) const;

private:
	UPROPERTY()
	class UDataTable* CharacterStats;
	
};

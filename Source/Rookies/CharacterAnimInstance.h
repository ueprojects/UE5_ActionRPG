// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CharacterAnimInstance.generated.h"

/**
 * 
 */
DECLARE_MULTICAST_DELEGATE(OnAttackHit);
UCLASS()
class ROOKIES_API UCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UCharacterAnimInstance();

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void PlayAttackMontage();
	void JumpToSection(int32 SectionIndex);
	
	FName GetAttackMontageName(int32 SectionIndex);

private:
	UPROPERTY(BlueprintReadOnly, Category="Anim", meta=(AllowPrivateAccess))
	float Speed;

	UPROPERTY(BlueprintReadOnly, Category="Anim", meta=(AllowPrivateAccess))
	bool IsFalling;

	UPROPERTY(BlueprintReadOnly, Category="Anim", meta=(AllowPrivateAccess))
	UAnimMontage* AttackMontage;

	UFUNCTION()
	void AnimNotify_AttackHit();

	UPROPERTY(BlueprintReadOnly, Category="Anim", meta=(AllowPrivateAccess))
	float Vertical;

	UPROPERTY(BlueprintReadOnly, Category="Anim", meta=(AllowPrivateAccess))
	float Horizontal;

public:
	OnAttackHit OnAttackHit; 
};

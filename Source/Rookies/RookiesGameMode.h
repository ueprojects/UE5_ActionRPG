// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RookiesGameMode.generated.h"

UCLASS(minimalapi)
class ARookiesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARookiesGameMode();
};



